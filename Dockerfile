FROM mcr.microsoft.com/dotnet/core/sdk:2.2-bionic

RUN apt-get update \
        && apt-get -y install python3-pip zip \
        && pip3 install awscli --upgrade
